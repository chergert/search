/* search-application.c
 *
 * Copyright 2021 Christian Hergert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "search-application.h"
#include "search-window.h"

struct _SearchApplication
{
  AdwApplication parent_instance;
};

G_DEFINE_TYPE (SearchApplication, search_application, ADW_TYPE_APPLICATION)

SearchApplication *
search_application_new (gchar *application_id,
                        GApplicationFlags  flags)
{
  return g_object_new (SEARCH_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
search_application_finalize (GObject *object)
{
  SearchApplication *self = (SearchApplication *)object;

  G_OBJECT_CLASS (search_application_parent_class)->finalize (object);
}

static void
search_application_activate (GApplication *app)
{
  GtkWindow *window;

  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (SEARCH_TYPE_WINDOW,
                           "application", app,
                           NULL);

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (window);
}

static void
search_application_startup (GApplication *app)
{
  g_autoptr(GtkCssProvider) provider = gtk_css_provider_new ();

  G_APPLICATION_CLASS (search_application_parent_class)->startup (app);

  gtk_icon_theme_add_resource_path (gtk_icon_theme_get_for_display (gdk_display_get_default ()),
                                    "/icons");

  gtk_css_provider_load_from_resource (provider, "/org/example/App/stylesheet.css");
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (provider),
                                              GTK_STYLE_PROVIDER_PRIORITY_THEME+1);
}

static void
search_application_class_init (SearchApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = search_application_finalize;

  app_class->activate = search_application_activate;
  app_class->startup = search_application_startup;
}

static void
search_application_show_about (GSimpleAction *action,
                               GVariant      *parameter,
                               gpointer       user_data)
{
  SearchApplication *self = SEARCH_APPLICATION (user_data);
  GtkWindow *window = NULL;
  const gchar *authors[] = {"Christian Hergert", NULL};

  g_return_if_fail (SEARCH_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  gtk_show_about_dialog (window,
                         "program-name", "search",
                         "authors", authors,
                         "version", "0.1.0",
                         NULL);
}


static void
search_application_init (SearchApplication *self)
{
  GSimpleAction *quit_action = g_simple_action_new ("quit", NULL);
  g_signal_connect_swapped (quit_action, "activate", G_CALLBACK (g_application_quit), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (quit_action));

  GSimpleAction *about_action = g_simple_action_new ("about", NULL);
  g_signal_connect (about_action, "activate", G_CALLBACK (search_application_show_about), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (about_action));

  const char *accels[] = {"<primary>q", NULL};
  gtk_application_set_accels_for_action (GTK_APPLICATION (self), "app.quit", accels);
}

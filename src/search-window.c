/* search-window.c
 *
 * Copyright 2021 Christian Hergert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtksourceview/gtksource.h>

#include "search-config.h"
#include "search-window.h"

struct _SearchWindow
{
  AdwApplicationWindow  parent_instance;
  AdwHeaderBar         *header_bar;
  GtkSearchEntry       *search_entry;
  GtkListBox           *list;
  GtkListBoxRow        *top;
  GtkSourceView        *sourceview;
  GtkSourceBuffer      *sourcebuffer;
};

G_DEFINE_TYPE (SearchWindow, search_window, ADW_TYPE_APPLICATION_WINDOW)

static void
search_window_class_init (SearchWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/App/search-window.ui");
  gtk_widget_class_bind_template_child (widget_class, SearchWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, SearchWindow, search_entry);
  gtk_widget_class_bind_template_child (widget_class, SearchWindow, list);
  gtk_widget_class_bind_template_child (widget_class, SearchWindow, sourcebuffer);
  gtk_widget_class_bind_template_child (widget_class, SearchWindow, sourceview);
  gtk_widget_class_bind_template_child (widget_class, SearchWindow, top);

  g_type_ensure (GTK_SOURCE_TYPE_VIEW);
}

static void
search_window_init (SearchWindow *self)
{
  GtkSourceLanguage *lang;
  GtkSourceStyleScheme *scheme;

  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");

  lang = gtk_source_language_manager_get_language (gtk_source_language_manager_get_default (), "c");
  scheme = gtk_source_style_scheme_manager_get_scheme (gtk_source_style_scheme_manager_get_default (), "Adwaita");
  gtk_source_buffer_set_language (self->sourcebuffer, lang);
  gtk_source_buffer_set_style_scheme (self->sourcebuffer, scheme);

  gtk_text_buffer_set_text (GTK_TEXT_BUFFER (self->sourcebuffer), "\
#include \"config.h\"\n\
\n\
#include <gtk/gtk.h>\n\
\n\
#include \"my-widget.h\"\n\
\n\
G_DEFINE_TYPE (MyWidget, my_widget, GTK_TYPE_WIDGET)\n\
\n\
static void\n\
my_widget_class_init (MyWidgetClass *klass)\n\
{\n\
}\n\
\n\
static void\n\
my_widget_init (MyWidget *self)\n\
{\n\
}\n\
\n\
MyWidget *\n\
my_widget_new (void)\n\
{\n\
  return g_object_new (MY_TYPE_WIDGET, NULL);\n\
}", -1);

  gtk_widget_grab_focus (GTK_WIDGET (self->search_entry));
  gtk_list_box_select_row (self->list, self->top);
}
